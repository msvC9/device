Rails.application.routes.draw do
  resources :chat_rooms, only: [:new, :create, :show, :index]
  root 'chat_rooms#index'

  devise_for :users
  #root to: 'pages#index'
  get '/secret', to: 'pages#secret', as: :secret
  
  mount ActionCable.server => '/cable'
end
